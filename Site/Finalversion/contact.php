<?php

include 'backend/connectToDatabase.php';

session_start();

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/contactstyle.css">
    <link rel="icon" href="img/light-bulb-7.png">
</head>
<header>
    <div class="navwrapper">
        <div class="headerlogo">
            <p id="headerknowitall">The KnowItAll</p>
        </div>
        <div class="navitems">
            <ul>
                <a href="index.php"><div><li>Home</li></div></a>
                <a href="archief.php"><div><li>Archief</li></div></a>
                <a href="overons.php"><div><li>Over Ons</li></div></a>
                <a href="contact.php"><div><li class="active">Contact</li></div></a>
                <a href="inloggen.php"><div><li>Log In</li></div></a>
            </ul>
        </div>
    </div>
</header>
<body>
<script>
    function stuur() {
        console.log("yeezy");


    }

</script>
<div class="container" id="container1">
    <h1 class="formtitle">Stuur een bericht!</h1>
    <form action="contactsend.php" method="post">
        <div class="row">
            <div class="input">
                <input type="text" id="fname" name="firstname" placeholder="voornaam" required>
            </div>
        </div>
        <div class="row">
            <div class="input">
                <input type="text" id="lname" name="lastname" placeholder="achternaam" required>
            </div>
        </div>
        <div class="row">
            <div class="input">
                <input type="text" id="email" name="email" placeholder="e-mail adres" required>
            </div>
        </div>
        <div class="row">
            <div class="input">
                <textarea id="subject" class="textarea" name="bericht" placeholder="Plaats hier je bericht" required></textarea>
            </div>
        </div>
        <div class="row">
            <input type="submit" name="submit" onclick="stuur(this)" value="Verzenden">
        </div>
    </form>
</div>

<div class="container" id="container2" style="display: none;">
    <form>
        <h1 style="margin-top: 10px; ">Uw weetje is verzonden ! </h1>
        <input type="submit" value="Back">
    </form>
</div>


</body>

<footer>   <p id="footertext">Gemaakt door: Youssef, Gerben, Yanick, Thomas, Maurice</p>
    <p id="footertext2">&copy; Copyright by The KnowItAll, designed by YGYTM</p>
    <div class="smediabuttons">
        <a target="blank" href="https://www.facebook.com/search/top/?q=The%20knowitall"><img class="smediabutton" src="img/fbicon.png"></a>
        <a target="blank" href="https://twitter.com/"><img class="smediabutton" src="img/twittericon.png"></a>
        <a target="blank" href="http://www.mobilephoneemulator.com/"><img class="smediabutton" src="img/telephoneicon.png"></a>
    </div>
</footer>

</html>