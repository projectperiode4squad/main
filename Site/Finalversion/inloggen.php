<?php
// Start the sessio

    session_start();

    $dbserver = "localhost";
    $dbuser = "weetje";
    $dbpass = "Weetje12345!";
    $dbname = "weetje";

    //Maak connectie met de database
    $conn = new mysqli($dbserver, $dbuser, $dbpass, $dbname);

    //Controleer connectie
    if($conn->connect_error) {
        die("<hr>Connectie mislukt: <br>" . $conn->connect_error . "<br>");
    }

    if(isset($_POST['submit'])) {

        $useremail = $conn->real_escape_string($_POST['email']);
        $password = $_POST['password'];

        $SQL = "SELECT role FROM users WHERE email='$useremail'";
        $roleresult = $conn->query($SQL);
        $rolerow = $roleresult->fetch_assoc();

        $role = $rolerow['role'];

        //Gebruiker wilt inloggen
        $sql = "SELECT email, password, role FROM users WHERE email = '$useremail'";
        if($result = $conn->query($sql)) {
            if($result->num_rows == 1) {
                $row = $result->fetch_row();
                $hashed_password = $row[1];
                if(password_verify($password, $hashed_password)) {
                    $loggedin = true;
                    //Set session vars
                    $_SESSION['email'] = $useremail;
                    $_SESSION['password'] = $password;
                    $_SESSION['role'] = $role;
                    header("location: index.php");

                }else{
                    $errormessage = 'Er is iets foutgegaan';
                }
            }else{
                $errormessage = 'Er is iets foutgegaan';
            }
        }else{
            $errormessage = 'Er is iets foutgegaan';
        }
    }
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="icon" href="img/light-bulb-7.png">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/inloggenstyle.css">
</head>
<header>
    <div class="navwrapper">
        <div class="headerlogo">
            <p id="headerknowitall">The KnowItAll</p>
        </div>
        <div class="navitems">
            <ul>
                <a href="index.php"><div><li>Home</li></div></a>
                <a href="archief.php"><div><li>Archief</li></div></a>
                <a href="overons.php"><div><li>Over Ons</li></div></a>
                <a href="contact.php"><div><li>Contact</li></div></a>
                <a href="inloggen.php"><div><li class="active">Log In</li></div></a>
            </ul>
        </div>
    </div>
</header>
<body>

    <div class="container">
        <?php
            if(!isset($_SESSION['email'])){
                echo'
                <h1 class="formtitle">Log hier in</h1>
                <form action="inloggen.php" method="post">

                    <div class="row">
                        <div class="input">
                            <input type="email" name="email" placeholder="voorbeeld@provider.nl" required>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input">
                            <input type="password" name="password" placeholder="Wachtwoord" required>
                        </div>
                    </div>

                    <div class="row">
                        <input type="submit" name="submit" value="Log in!">
                        <a href="registrationpage.php"><p class="aanmelden">Meld je aan!</p></a>
                    </div>
                    <h1 class="foutmelding">'; if(isset($errormessage)){echo $errormessage;} echo'</h1>
                </form>';}else{
                echo'<h2 class="logouttitle">U bent al ingelogd als</h2>'; echo '<h2 class="email">' .$_SESSION['email'] . '</h2>
                    <hr><br>  
                    <a href="logout.php"><p class="aanmelden logout">Log uit!</p></a></div>';
            }
        ?>
    </div>

</body>
<footer>   <p id="footertext">Gemaakt door: Youssef, Gerben, Yanick, Thomas, Maurice</p>
    <p id="footertext2">&copy; Copyright by The KnowItAll, designed by YGYTM</p>
    <div class="smediabuttons">
        <a target="blank" href="https://www.facebook.com/search/top/?q=The%20knowitall"><img class="smediabutton" src="img/fbicon.png"></a>
        <a target="blank" href="https://twitter.com/"><img class="smediabutton" src="img/twittericon.png"></a>
        <a target="blank" href="http://www.mobilephoneemulator.com/"><img class="smediabutton" src="img/telephoneicon.png"></a>
    </div>
</footer>
</html>
