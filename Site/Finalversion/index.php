<?php

    include ("backend/connectToDatabase.php");

    session_start();

    $date = date("Y/m/d");

    $randomSql = "SELECT * FROM weetjestabel WHERE datum = '$date' AND permission='1' LIMIT 1";

    $date = date("d/m/Y");

    $result = $conn->query($randomSql);
    $row = $result->fetch_assoc();

    $rand = mt_rand(1, 4);


?>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/indexcss.css">
        <meta charset="UTF-8">
        <meta name="language" content="dutch">
        <link rel="icon" href="img/light-bulb-7.png">
        <meta name="author" content="yanick palmers, gerben schipper, maurice, thomas">
        <meta name="description" content="voertuig feiten">
        <meta name="keywords" content="know it all feitjes feit voertuigen">
        <meta name="copyright" content="copyright">
        <title>KnowItAll</title>
        <style>
            body{
                background: url("img/auto/auto<?php echo $rand; ?>.jpg") no-repeat center center fixed;
                background-size: cover;
            }
        </style>
    </head>
    <header>
        <div class="navwrapper">
            <div class="headerlogo">
                <p id="headerknowitall">The KnowItAll</p>
            </div>
            <div class="navitems">
                <ul>
                    <a href="index.php"><div><li class="active">Home</li></div></a>
                    <a href="archief.php"><div><li>Archief</li></div></a>
                    <a href="overons.php"><div><li>Over Ons</li></div></a>
                    <a href="contact.php"><div><li>Contact</li></div></a>
                    <a href="inloggen.php"><div><li>Log In</li></div></a>
                </ul>
            </div>
        </div>
    </header>
    <body>
    <script>
        var clickcount = 1;
        function myFunction() {
            clickcount =  parseInt(clickcount)+parseInt(1);
            var x = document.getElementById("myDIV");
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }


            if (clickcount % 2 == 0) {
                document.getElementById("meerfeitje").innerHTML = "lees minder";
            } else {
                document.getElementById("meerfeitje").innerHTML = "lees meer";
            }
        }
    </script>
        <div id="feitjeindex" class="feitje">
            <p class="weetje_title"><?php echo 'Weetje van: ' . $date ?></p>
            <br>
            <p class="weetje">
            <?php
                if(isset($row['weetje'])){
                    echo $row['weetje'];
                    echo '<div id="myDIV" style="display:none">' . $row["info"] . '
                    </div>';
                    if(!$row['info'] == "") {
                        echo '<button id="meerfeitje" onclick="myFunction();">Lees meer</button>';
                    }
                }else{echo'Er is geen weetje van vandaag, kom morgen terug!';}
            ?>
            </p>
            <?php if(isset($_SESSION['email'])){ echo'<a href="weetjeinsturen.php"><p class="aanmelden">Stuur een weetje in!</p></a>';}?>
            <?php
                if(isset($_SESSION['email'])){
            if($_SESSION['role'] == 'admin'){echo'<a href="gebruikerlijst.php"><p class="aanmelden">ga naar gebruikers lijst</p></a>';}}?>
        </div>

    </body>
    <footer>   <p id="footertext">Gemaakt door: Youssef, Gerben, Yanick, Thomas, Maurice</p>
        <p id="footertext2">&copy; Copyright by The KnowItAll, designed by YGYTM</p>
        <div class="smediabuttons">
            <a target="blank" href="https://www.facebook.com/search/top/?q=The%20knowitall"><img class="smediabutton" src="img/fbicon.png"></a>
            <a target="blank" href="https://twitter.com/"><img class="smediabutton" src="img/twittericon.png"></a>
            <a target="blank" href="http://www.mobilephoneemulator.com/"><img class="smediabutton" src="    img/telephoneicon.png"></a>
        </div>
    </footer>
</html>