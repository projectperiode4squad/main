<?php session_start(); ?>

<!DOCTYPE html>
<html lang="nl">
  <head>
      <title>KnowItAll</title>
      <link rel="icon" href="img/light-bulb-7.png">
    <meta charset="UTF-8">
	<link rel="stylesheet" href="css/overons.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <header>
      <div class="navwrapper">
          <div class="headerlogo">
              <p id="headerknowitall">The KnowItAll</p>
          </div>
          <div class="navitems">
              <ul>
                  <a href="index.php"><div><li>Home</li></div></a>
                  <a href="archief.php"><div><li>Archief</li></div></a>
                  <a href="overons.php"><div><li class="active">Over Ons</li></div></a>
                  <a href="contact.php"><div><li>Contact</li></div></a>
                  <a href="inloggen.php"><div><li>Log In</li></div></a>
              </ul>
          </div>
      </div>
  </header>
  <body>
  <script>
      var timesClicked = 1;
      function meerlezen() {
          timesClicked =  parseInt(timesClicked)+parseInt(1);
          var title = "The KnowItAll is:";
          var text = "Een Nederlandse archiefbeheerder dat allerlei weetjes en informatie heeft over voertuigen. ";
          var text2 = "Omdat er steeds minder mensen fysiek naar ons archief kwamen hebben we er een website van gemaakt ";
          var text3 = "op deze website kunt u feitjes zien over voertuigen gesorteerd op datum. op de archief pagina kunt u alle feitjes bekijken van elke dag. meer over ons bedrijf: ";
          //console.log("" + timesClicked);


          //timesClicked += 1;

          if (timesClicked % 2 == 0) {
              document.getElementById("overons").innerHTML = "" + title;
              document.getElementById("overonstext").innerHTML = "" + text + text2 + text3;
              document.getElementById("meerlezen").innerHTML = "minder lezen";
          } else {
              document.getElementById("overons").innerHTML = "Wij zijn The KnowItAll.";
              document.getElementById("overonstext").innerHTML = "Wij beheren een archief over voertuigen en een database met weetjes over oude voertuigen.";
              document.getElementById("meerlezen").innerHTML = "meer lezen";
          }
      }
  </script>
   <div class="overonsdiv">
    <p id="title_overons">Over Ons</p>
       <h3 id="overons">Wij zijn The KnowItAll.</h3><br>
	    <p id="overonstext">Wij beheren een archief over voertuigen en<br>
            een database met weetjes over oude voertuigen.<br></p>
	    <br>
		Contact met ons opnemen?<br>
	    <a href="contact.php" class="contactlink">Klik hier!</a><br>
        <p id="meerlezen" onclick="meerlezen()">meer lezen</p>

   </div>
  </body>
  <footer>   <p id="footertext">Gemaakt door: Youssef, Gerben, Yanick, Thomas, Maurice</p>
      <p id="footertext2">&copy; Copyright by The KnowItAll, designed by YGYTM</p>
      <div class="smediabuttons">
          <a target="blank" href="https://www.facebook.com/search/top/?q=The%20knowitall"><img class="smediabutton" src="img/fbicon.png"></a>
          <a target="blank" href="https://twitter.com/"><img class="smediabutton" src="img/twittericon.png"></a>
          <a target="blank" href="http://www.mobilephoneemulator.com/"><img class="smediabutton" src="img/telephoneicon.png"></a>
      </div>
  </footer>
</html>