<?php

include 'backend/connectToDatabase.php';

session_start();

if(!isset($_SESSION['email'])){
    header("location: index.php");
};

$ID = $_GET['ID'];

$SQL = "SELECT * FROM weetjestabel WHERE ID=$ID";

$result2 = $conn->query($SQL);
$row2 = $result2->fetch_assoc();


if(isset($_POST['submit'])){

    $sql = "UPDATE weetjestabel SET permission='1' WHERE ID=$ID";

    $result = $conn->query($sql);

    $message = 'Uw weetje:<br><br>' . $row2['weetje'] . '<br><br>is geaccepteerd';

    $headers =  'MIME-Version: 1.0' . "\r\n";
    $headers .= 'From: Your name <' . $_SESSION['email'] . '>' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

    mail($row2['email'],'goedgekeurd',$message,$headers);

    header("location: weetjeinsturen.php");

}

if(isset($_POST['cancel'])){

    $sql = "DELETE FROM weetjestabel WHERE ID=$ID";

    $result = $conn->query($sql);

    $headers =  'MIME-Version: 1.0' . "\r\n";
    $headers .= 'From: Your name <' . $_SESSION['email'] . '>' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

    mail($row2['email'],'afgekeurd','Sorry, uw weetje is niet geaccepteerd',$headers);

    header("location: weetjeinsturen.php");
}


?>

<!DOCTYPE html>
<html>
<head>
    <link rel="icon" href="img/light-bulb-7.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/accept.css">
    <meta charset="UTF-8">
    <meta name="language" content="dutch">
    <meta name="author" content="yanick palmers, gerben schipper, maurice, thomas">
    <meta name="description" content="voertuig feiten">
    <meta name="keywords" content="know it all feitjes feit voertuigen">
    <meta name="copyright" content="copyright">
    <title>KnowItAll</title>
</head>
<header>
    <div class="navwrapper">
        <div class="headerlogo">
            <p id="headerknowitall">The KnowItAll</p>
        </div>
        <div class="navitems">
            <ul>
                <a href="index.php"><div><li>Home</li></div></a>
                <a href="archief.php"><div><li>Archief</li></div></a>
                <a href="overons.php"><div><li>Over Ons</li></div></a>
                <a href="contact.php"><div><li>Contact</li></div></a>
                <a href="inloggen.php"><div><li>Log In</li></div></a>
            </ul>
        </div>
    </div>
</header>

<body>

<div id="feitjeindex" class="feitje">
    <p class="weetje_title">Weet u zeker dat u dit weetje wilt accepteren?</p>
    <br>
    <form action="" method="post">
        <div class="row">
            <input class="submit" type="submit" name="submit" value="Ja zeker">
        </div>
        <div class="row">
            <input class="cancel" type="submit" name="cancel" value="nee, verwijder">
        </div>
            <input type="hidden" name="id" value="">
    </form>
</div>


</body>
<footer style="position: relative; bottom: -50px;">

    <p id="footertext">Gemaakt door: Youssef, Gerben, Yanick, Thomas, Maurice</p>
    <p id="footertext2">&copy; Copyright by The KnowItAll, designed by YGYTM</p>
    <div class="smediabuttons">
        <a target="blank" href="https://www.facebook.com/search/top/?q=The%20knowitall"><img class="smediabutton" src="img/fbicon.png"></a>
        <a target="blank" href="https://twitter.com/"><img class="smediabutton" src="img/twittericon.png"></a>
        <a target="blank" href="http://www.mobilephoneemulator.com/"><img class="smediabutton" src="img/telephoneicon.png"></a>
    </div>
</footer>
</html>