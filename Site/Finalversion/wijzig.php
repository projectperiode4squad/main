<?php

    include 'backend/connectToDatabase.php';

    session_start();

    if(!isset($_SESSION['email'])){
        header("location: inloggen.php");
    };

    if(isset($_SESSION['email'])){

        $sql = "SELECT * FROM weetjestabel WHERE ID=" . $_GET['ID'];

        $result = $conn->query($sql);
        $row = $result->fetch_assoc();
    }

    if(isset($_POST['submit'])){

        $date = htmlspecialchars($_POST['date']);
        $email = htmlspecialchars($_SESSION['email']);
        $weetje = htmlspecialchars($_POST['weetje']);
        $info = htmlspecialchars($_POST['info']);

        $date = $conn->real_escape_string($date);
        $email = $conn->real_escape_string($email);
        $weetje = $conn->real_escape_string($weetje);
        $info = $conn->real_escape_string($info);


        $sql = "UPDATE weetjestabel SET datum='" . $date . "',weetje='" . $weetje . "', 
        info='" . $info ."' WHERE ID=" . $_GET['ID'];
        $result = $conn->query($sql);

        header("location: weetjeinsturen.php");
    }

?>
<!DOCTYPE html>
<html>
<head>
    <link rel="icon" href="img/light-bulb-7.png">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/contactstyle.css">
</head>
<header>
    <div class="navwrapper">
        <div class="headerlogo">
            <p id="headerknowitall">The KnowItAll</p>
        </div>
        <div class="navitems">
            <ul>
                <a href="index.php"><div><li>Home</li></div></a>
                <a href="archief.php"><div><li>Archief</li></div></a>
                <a href="overons.php"><div><li>Over Ons</li></div></a>
                <a href="contact.php"><div><li>Contact</li></div></a>
                <a href="inloggen.php"><div><li>Log In</li></div></a>
            </ul>
        </div>
    </div>
</header>
<body>
    <div class="container">
         <h1 class="formtitle">Wijzig je weetje!</h1>
              <form action="" method="post">
                  <input name="date" type="date" value="<?php echo $row['datum']; ?>" required>
                   <div class="row">
                        <div class="input">
                            <textarea id="subject" class="textarea" name="weetje" required><?php echo $row['weetje']; ?></textarea>
                            <textarea id="subject" class="textarea" name="info" placeholder="Plaats hier meer informatie!"><?php echo $row['info']; ?></textarea>
                        </div>
                   </div>
                  <div class="row">
                        <input type="submit" name="submit" value="Wijzig">
                   </div>
              </form>
        </div>
</body>
<footer>
    <p id="footertext">Gemaakt door: Youssef, Gerben, Yanick, Thomas, Maurice</p>
    <p id="footertext2">&copy; Copyright by The KnowItAll, designed by YGYTM</p>
    <div class="smediabuttons">
        <a target="blank" href="https://www.facebook.com/search/top/?q=The%20knowitall"><img class="smediabutton" src="img/fbicon.png"></a>
        <a target="blank" href="https://twitter.com/"><img class="smediabutton" src="img/twittericon.png"></a>
        <a target="blank" href="http://www.mobilephoneemulator.com/"><img class="smediabutton" src="img/telephoneicon.png"></a>
    </div>
</footer>

</html>