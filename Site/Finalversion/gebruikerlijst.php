<?php

include ("backend/connectToDatabase.php");

session_start();

if(!isset($_SESSION['email'])){
    header("location: inloggen.php");
}

$lijst = [];

if(isset($_SESSION['email'])){
    if($_SESSION['role'] == 'admin'){
    }else{
        header("location: index.php");
    }
}else {
    header("location: index.php");
}

if(isset($_GET['target'])) {
    switch ($_GET['target']) {
        case 'delete':
            $userid = $_GET['delete'];

            $SQL = "DELETE FROM users WHERE ID= " . $userid;
            $conn->query($SQL);
            header("location: gebruikerlijst.php");

        break;

        case 'upgrade' :
            $userid = $_GET['upgrade'];

            $query = "SELECT * FROM users WHERE ID= " . $userid;
            $result=$conn->query($query);
            $row=$result->fetch_assoc();

            if($row['role'] == 'admin'){
                $SQL = "UPDATE users SET role = 'user' WHERE ID =" . $userid;
                $conn->query($SQL);
                break;
            }

            $SQL = "UPDATE users SET role = 'admin' WHERE ID =" . $userid;
            $conn->query($SQL);
        break;

        case 'aanpassen' :
            $userid = $_GET['upd'];

            $query = "SELECT * FROM users WHERE ID= " . $userid;
            $result=$conn->query($query);
            $row=$result->fetch_assoc();

            if($row['role'] == 'blocked'){
                $SQL = "UPDATE users SET role = 'user' WHERE ID =" . $userid;
                $conn->query($SQL);
                break;
            }

            $SQL = "UPDATE users SET role = 'blocked' WHERE ID =" . $userid;
            $conn->query($SQL);
        break;

    }

}

$SQL = 'SELECT * FROM users ORDER BY role ASC ';
$result = $conn->query($SQL);
//$row = $result->fetch_assoc();

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $lijst[] = '<tr>
		<td>'.$row['email']. '</td><br>
		<td id="role">'.$row['role'].'</td>
		<br><br>
		<td><a href="gebruikerlijst.php?target=aanpassen&upd='.$row['ID'].'"><img src="img/blocked.png" class="imgaanpassen" name="blokeer gebruiker" title="blokkeren/deblokeren" alt="blokeer gebruiker"></a></td>
		<td><a href="gebruikerlijst.php?target=upgrade&upgrade='.$row['ID'].'"><img src="img/admin.png" class="imgaanpassen" name="gebruiker admin maken" title="promotie/demotie" alt="gebruiker admin maken"></a></td></tr>
		<td><a href="gebruikerlijst.php?target=delete&delete='.$row['ID'].'"><img src="img/delete1.png" class="imgaanpassen" name="gebruiker verwijderen" title="gebruiker verwijderen" alt="gebruiker verwijderen"></a></td></tr>
		';
    }
} else {
    echo "";
    };

$conn->close();


?>

<!DOCTYPE html>
<html>
<head>
    <link rel="icon" href="img/light-bulb-7.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/contactstyle.css">
    <meta charset="UTF-8">
    <meta name="language" content="dutch">
    <meta name="author" content="yanick palmers, gerben schipper, maurice, thomas">
    <meta name="description" content="voertuig feiten">
    <meta name="keywords" content="know it all feitjes feit voertuigen">
    <meta name="copyright" content="copyright">
    <title>KnowItAll</title>
    <style>
        body{
            background: url("img/admin.jpg") no-repeat center center fixed;
            background-size: cover;
        }
    </style>
</head>
<header>
    <div class="navwrapper">
        <div class="headerlogo">
            <p id="headerknowitall">The KnowItAll</p>
        </div>
        <div class="navitems">
            <ul>
                <a href="index.php"><div><li>Home</li></div></a>
                <a href="archief.php"><div><li>Archief</li></div></a>
                <a href="overons.php"><div><li>Over Ons</li></div></a>
                <a href="contact.php"><div><li>Contact</li></div></a>
                <a href="inloggen.php"><div><li>Log In</li></div></a>
            </ul>
        </div>
    </div>
</header>

<body>
<div id="gebruikerlijst" class="feitje">
<?php

if(isset($lijst)){
    foreach($lijst as $key => $regel){
        echo '<div id="weetje" class="feitje">
                <br>
                <p class="weetje">'. $regel .'</p>
            </div><hr class="lijntje">';
    }

} ?>
</div>
</body>
<footer>   <p id="footertext">Gemaakt door: Youssef, Gerben, Yanick, Thomas, Maurice</p>
    <p id="footertext2">&copy; Copyright by The KnowItAll, designed by YGYTM</p>
    <div class="smediabuttons">
        <a target="blank" href="https://www.facebook.com/search/top/?q=The%20knowitall"><img class="smediabutton" src="img/fbicon.png"></a>
        <a target="blank" href="https://twitter.com/"><img class="smediabutton" src="img/twittericon.png"></a>
        <a target="blank" href="http://www.mobilephoneemulator.com/"><img class="smediabutton" src="img/telephoneicon.png"></a>
    </div>
</footer>
</html>