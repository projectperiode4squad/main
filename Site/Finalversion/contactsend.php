<?php

include 'backend/connectToDatabase.php';

session_start();

if(!isset($_SESSION['email'])){
    header("location: inloggen.php");
};

if(isset($_POST['submit'])){

    $bericht = $_POST['bericht'];
    $bericht = $conn->real_escape_string($bericht);


    $name = $_POST['firstname'] . " " . $_POST['lastname'];
    $email = $_POST['email'];
    $date = date("d/m/Y") . " " . date("h:i:s");
    $message = $bericht . "\r\n" . "\r\n" . "Dit formulier is verzonden door " . $name . " om " . $date . "\r\n" . "Email: $email";



    //Hier de mailfunctie
    $to = "thomasdewee@outlook.com";
    $subject = "Dit is een contactformulier";
    $fact = $message;
    $headers = "From: webmaster@example.com";

    mail($to, $subject, $fact, $headers);

    header("location: succes.php");
}
?>

