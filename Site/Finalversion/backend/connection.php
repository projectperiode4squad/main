<?php

    $dbserver = "localhost";
    $dbuser = "project4user";
    $dbpass = "project4user";
    $dbname = "project4";

    //Connectie met Database maken
    $conn = new mysqli($dbserver, $dbuser, $dbpass, $dbname);

    //Controleer de connectie
    if($conn->connect_error) {
        die("<hr>Verbinding met database mislukt: <br>" . $conn->connect_error . "<hr>");
    }

?>