<?php

include 'backend/connectToDatabase.php';

session_start();

if(!isset($_SESSION['email'])){
    header("location: inloggen.php");
};

if(isset($_POST['submit'])){

    $date = htmlspecialchars($_POST['date']);
    $email = htmlspecialchars($_SESSION['email']);
    $weetje = htmlspecialchars($_POST['weetje']);
    $info = htmlspecialchars($_POST['info']);

    $date = $conn->real_escape_string($date);
    $email = $conn->real_escape_string($email);
    $weetje = $conn->real_escape_string($weetje);
    $info = $conn->real_escape_string($info);

    $permission = "";

    if($_SESSION['role'] == 'admin'){

        $permission = '1';
    }
    else{

        $permission = '0';
    }


    $sql = "INSERT INTO weetjestabel (email, datum, weetje, info, permission)
            VALUES ('$email','$date','$weetje', '$info', '$permission')";

    $conn->query($sql);

    echo $conn->error;

    header("location: weetjeinsturen.php");
}
?>
