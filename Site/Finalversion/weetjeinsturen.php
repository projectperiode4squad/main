<?php

    include 'backend/connectToDatabase.php';

    session_start();

    if(!isset($_SESSION['email'])){
        header("location: inloggen.php");
    };

    if(isset($_SESSION['email'])){


        $email = $_SESSION['email'];

        $sql = "SELECT * FROM weetjestabel";

        $result = $conn->query($sql);
        $row = $result->fetch_assoc();

        $sql2 = "SELECT * FROM weetjestabel WHERE email='$email' AND permission='1'";

        $result2 = $conn->query($sql2);
        $row2 = $result2->fetch_assoc();

        $sql3 = "SELECT * FROM weetjestabel WHERE email='$email' AND permission='0'";

        $result3 = $conn->query($sql3);
        $row3 = $result3->fetch_assoc();

    }

if($_SESSION["role"] == "blocked"){
        $blocked = "disabled";
        $blockedreason = "Dit accaunt is geblokeerd omdat er ongepaste weetjes zijn upgeload en de admin heeft besloten om uw rechten van een weetje plaatsen af te nemen";
        $blockeduser = '<form action="stuur.php" method="post"><br><br><textarea id="subject" class="textarea" name="info" placeholder=" 
        '. $blockedreason .' " ' . $blocked . '></textarea></form>';
}else{
    $blocked="";
    $blockedreason="Plaats hier meer informatie!";
    $blockeduser = '<form action="stuur.php" method="post">
                    <h1 class="formtitle">Stuur je weetje in!</h1>
                    <input name="date" type="date" required ' . $blocked . '>
                    <div class="row">
                        <div class="input">
                            <textarea id="subject" class="weetjearea" name="weetje" placeholder="Plaats hier je weetje!" required ' . $blocked . '></textarea>
                            <textarea id="subject" class="textarea" name="info" placeholder=" '. $blockedreason .' " ' . $blocked . '></textarea>
                        </div>
                    </div>
                        <input type="submit" name="submit" value="Verzenden">
                    
                    </form>';
}
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="icon" href="img/light-bulb-7.png">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/contactstyle.css">
</head>
<header>
    <div class="navwrapper">
        <div class="headerlogo">
            <p id="headerknowitall">The KnowItAll</p>
        </div>
        <div class="navitems">
            <ul>
                <a href="index.php"><div><li>Home</li></div></a>
                <a href="archief.php"><div><li>Archief</li></div></a>
                <a href="overons.php"><div><li>Over Ons</li></div></a>
                <a href="contact.php"><div><li>Contact</li></div></a>
                <a href="inloggen.php"><div><li>Log In</li></div></a>
            </ul>
        </div>
    </div>
</header>
<body>
    <?php
        if(isset($_SESSION['email'])) {
            echo '
            <div class="container">
                '.$blockeduser.'
                <p class="weetje">';
            echo '<div id="weetje" class="feitje">
                <br>
                <p class="weetje">';
            
            if($_SESSION['role'] == 'user'){
                    do{
                        if(isset($row2['weetje'])){echo "<hr>" . $row2['datum'] . "<br><br>" . $row2['weetje'] . "<br><br>";}
                    }while ($row2 = $result2->fetch_assoc());
                    do{
                        if(isset($row3['weetje'])){echo "<hr>" . $row3['datum'] . "<br><br>" . $row3['weetje'] . "<br><br>
                        <a href='wijzig.php?ID=" . $row3['ID'] . "'><img src='img/modify.png' class='modify' title='aanpassen' alt='aanpassen'></a>
                        <a href='delete.php?ID=" . $row3['ID'] . "'><img src='img/delete.png' class='delete' title='verwijderen' alt='verwijderen'></a>
                        <br><h5>Nog niet goedgekeurd...</h5>";}
                    }while ($row3 = $result3->fetch_assoc());
            }

            if($_SESSION['role'] == 'admin') {
                do{
                    echo "<hr>" . $row['datum'] . "<br><br>" . $row['email'] . "<br><br>" . $row['weetje'] . "<br><br>
                    <a href='wijzig.php?ID=" . $row['ID'] . "'><img src='img/modify.png' class='modify' title='aanpassen' alt='aanpassen'></a>";
                    if($row['permission'] == '1'){echo "<a href='delete.php?ID=" . $row['ID'] . "'><img src='img/delete.png' class='delete' title='verwijderen' alt='verwijderen'></a>";}
                    if($row['permission'] == '0'){
                        echo "<a href='accepteren.php?ID=" . $row['ID'] . "'><img src='img/accept.png' class='delete' title='accepteren' alt='accepteren' name='accepteren'></a>";
                    }
                }while ($row = $result->fetch_assoc());
            }

            echo '</p>
            </div>';
        }
    ?>
    </div>
</body>
<footer>
    <p id="footertext">Gemaakt door: Youssef, Gerben, Yanick, Thomas, Maurice</p>
    <p id="footertext2">&copy; Copyright by The KnowItAll, designed by YGYTM</p>
    <div class="smediabuttons">
        <a target="blank" href="https://www.facebook.com/search/top/?q=The%20knowitall"><img class="smediabutton" src="img/fbicon.png"></a>
        <a target="blank" href="https://twitter.com/"><img class="smediabutton" src="img/twittericon.png"></a>
        <a target="blank" href="http://www.mobilephoneemulator.com/"><img class="smediabutton" src="img/telephoneicon.png"></a>
    </div>
</footer>

</html>