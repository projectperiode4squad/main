<?php

    session_start();

    $dbserver = "localhost";
    $dbuser = "weetje";
    $dbpass = "Weetje12345!";
    $dbname = "weetje";

    //Maak connectie met de database
    $conn = new mysqli($dbserver, $dbuser, $dbpass, $dbname);

    //Controleer connectie
    if($conn->connect_error) {
        die("<hr>Connectie mislukt: <br>" . $conn->connect_error . "<br>");
    }

    if(isset($_POST['submit'])) {

        $useremail = $conn->real_escape_string($_POST['email']);
        $password = $_POST['password'];

        //Password hashen en gebruiker toevoegen
        $password = password_hash($password, PASSWORD_DEFAULT);
        $sql = "INSERT INTO users (email, password, role) VALUES ('$useremail', '$password', 'user')";

        //Voer de SQL query uit
        if($conn->query($sql) === TRUE) {
            //Stuur de user naar de login page
            header("location: inloggen.php");
        } else {    
            echo "<hr>Er is een fout opgetreden: <br>" . $conn->error . "<hr>";
        }
    }
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="icon" href="img/light-bulb-7.png">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/inloggenstyle.css">
</head>
    <header>
        <div class="navwrapper">
            <div class="headerlogo">
                <p id="headerknowitall">The KnowItAll</p>
            </div>
            <div class="navitems">
                <ul>
                    <a href="index.php"><div><li>Home</li></div></a>
                    <a href="archief.php"><div><li>Archief</li></div></a>
                    <a href="overons.php"><div><li>Over Ons</li></div></a>
                    <a href="contact.php"><div><li>Contact</li></div></a>
                    <a href="inloggen.php"><div><li>Log In</li></div></a>
                </ul>
            </div>
        </div>
    </header>
<body>
<script>
function ietsanders(text) {
    var ww = document.getElementById("wachtwoord").value;
    var ww2 = document.getElementById("wachtwoord2").value;

    if (ww != ww2){
       // console.log("wacchtwoorden komen niet overeen");
        document.getElementById("meldjeaan").disabled = true;
        document.getElementById("meldjeaan").style.border = "1px solid red";
        document.getElementById("wachtwoord").style.border = "1px solid red";
        document.getElementById("wachtwoord2").style.border = "1px solid red";
        document.getElementById("foutwachtwoord").innerHTML = "de ingevulde wachtworden komen niet over heen";

    }
    if (ww == ww2){
        document.getElementById("meldjeaan").disabled = false;

    }
}
</script>
<div class="container">
    <h1 class="formtitle">Maak hier uw account aan</h1>
    <form action="registrationpage.php" method="post">

        <div class="row">
            <div class="input">
                <input type="email" name="email" placeholder="voorbeeld@provider.nl" required>
            </div>
        </div>

        <div class="row">
            <div class="input">
                <input type="password" name="password" placeholder="Wachtwoord" id="wachtwoord"required >
            </div>
            <div class="input">
                <input type="password" name="password2" placeholder="herhaal wachtwoord" id="wachtwoord2" onchange="ietsanders()" required >
            </div>
        </div>

        <div class="row">
            <input id="meldjeaan" type="submit" name="submit" value="Meld je aan!">
            <a href="inloggen.php"><p class="aanmelden">Log in!</p></a>
            <p id="foutwachtwoord"></p>

        </div>

    </form>
</div>
</body>
<footer>   <p id="footertext">Gemaakt door: Youssef, Gerben, Yanick, Thomas, Maurice</p>
    <p id="footertext2">&copy; Copyright by The KnowItAll, designed by YGYTM</p>
    <div class="smediabuttons">
        <a target="blank" href="https://www.facebook.com/search/top/?q=The%20knowitall"><img class="smediabutton" src="img/fbicon.png"></a>
        <a target="blank" href="https://twitter.com/"><img class="smediabutton" src="img/twittericon.png"></a>
        <a target="blank" href="http://www.mobilephoneemulator.com/"><img class="smediabutton" src="img/telephoneicon.png"></a>
    </div>
</footer>
</html>