<?php
$dbserver = "localhost";
$dbuser = "project4user";
$pass = "project4user";

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="style.css">
</head>
<header>
    <div class="navwrapper">
        <div class="headerlogo">
            <p id="headerknowitall">The KnowItAll</p>
        </div>
        <div class="navbar">
            <img class="usericon" src=../Header/img/user_male.png>
        </div>
        <div class="navitems">
            <ul>
                <a href="index.html"><li>Home</li></a>
                <a href="archief.html"><li>| Archief |</li></a>
                <a href="overons.html"><li>Over Ons |</li></a>
                <a href="contact.html"><li>Contact |</li></a>
                <a href="login.html"><li class="active">Log In </li></a>
            </ul>
        </div>
    </div>
</header>
<body>
<div class="container">
    <h1>Log hier in</h1>
    <form action="inloggen.php" method="post">

        <div class="row">
            <div class="input">
                <input type="email" name="email" placeholder="voorbeeld@provider.nl">
            </div>
        </div>

        <div class="row">
            <div class="input">
                <input type="password" id=""name="pass" placeholder="Wachtwoord">
            </div>
        </div>

        <div class="row">
            <input type="submit" value="Verzenden">
            <input type="reset" value="Reset">
        </div>
    </form>
</div>
</body>
<footer>   <p id="footertext">Gemaakt door: Youssef, Gerben, Yannick, Thomas, Maurice</p>
    <p id="footertext2">&copy; Copyright by The KnowItAll, designed by YGYTM</p>
    <div class="smediabuttons">
        <a target="blank" href="https://www.facebook.com/search/top/?q=The%20knowitall"><img class="smediabutton" src="../Header/img/fbicon.png"></a>
        <a target="blank" href="https://twitter.com/"><img class="smediabutton" src="../Header/img/twittericon.png"></a>
        <a target="blank" href="http://www.mobilephoneemulator.com/"><img class="smediabutton" src="../Header/img/telephoneicon.png"></a>
    </div>
</footer>
</html>
