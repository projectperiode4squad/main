<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="style.css">
</head>
    <header>
        <div class="navwrapper">
            <div class="headerlogo">
                <p id="headerknowitall">The KnowItAll</p>
            </div>
            <div class="navbar">
                <img class="usericon" src=../Header/img/user_male.png>
            </div>
            <div class="navitems">
                <ul>
                    <a href="index.html"><li>Home</li></a>
                    <a href="archief.html"><li>| Archief |</li></a>
                    <a href="overons.html"><li>Over Ons |</li></a>
                    <a href="contact.html"><li class="active">Contact |</li></a>
                    <a href="login.html"><li>Log In </li></a>
                </ul>
            </div>
        </div>
    </header>
    <body>
    <div class="container">
        <h1>Stuur je weetje in!</h1>
        <form action="" method="post">
            <div class="row">
                <div class="input">
                    <input type="text" id="fname" name="firstname" placeholder="Yanick">
            </div>
            </div>
            <div class="row">
                <div class="input">
                    <input type="text" id="lname" name="lastname" placeholder="Palmers">
                </div>
            </div>
            <div class="row">
            <div class="input">
                <input type="text" id="email" name="email" placeholder="yanickpalmers@gmail.com">
            </div>
            </div>
            <div class="row">
                <div class="input">
                    <textarea id="subject" class="textarea" name="subject" placeholder="Schrijf hier je weetje"></textarea>
                </div>
            </div>
            <div class="row">
                <input type="submit" value="Verzenden">
            </div>
        </form>
    </div>
</body>
    <footer>   <p id="footertext">Gemaakt door: Youssef, Gerben, Yannick, Thomas, Maurice</p>
        <p id="footertext2">&copy; Copyright by The KnowItAll, designed by YGYTM</p>
        <div class="smediabuttons">
            <a target="blank" href="https://www.facebook.com/search/top/?q=The%20knowitall"><img class="smediabutton" src="../Header/img/fbicon.png"></a>
            <a target="blank" href="https://twitter.com/"><img class="smediabutton" src="../Header/img/twittericon.png"></a>
            <a target="blank" href="http://www.mobilephoneemulator.com/"><img class="smediabutton" src="../Header/img/telephoneicon.png"></a>
        </div>
    </footer>
</html>